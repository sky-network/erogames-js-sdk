# JS Erogames Analytics

## How to add the script to a project

1. Download the script:  
   `https://gitlab.com/sky-network/erogames-js-sdk/-/blob/master/analytics/erogames-analytics-<x.y.z>.js`
2. Put downloaded script into the project.
3. Add the script to the `<head>` tag:
   ```html
   <head>
   ....
    <script src="path_to_the_script/erogames-analytics-<x.y.z>.min.js"></script>   
   ....
   </head>
   ```
4. Initialize Erogames Analytics:
   ```html
   <head>
   ....
    <script>
        const eroAnalytics = erogamesAnalytics.analytics()
        eroAnalytics.init({clientId: 'your_client_id'})
   </script>   
   ....
   </head>
   ```

### Log event

To override data used by Erogames Analytics internally, add `ea_` prefix to the param name. To add custom data, the
param name should not be started with `ea_` prefix.

```javascript
const event = "install"
eroAnalytics.logEvent(event).then(() => {
    // SUCCESS
}).catch((error) => {
    // ERROR
});
```

Log event with a payload.

```javascript
const event = "click"
const params = new Map();
params.set("ea_category", "banners");

eroAnalytics.logEvent(event, params).then(() => {
    // SUCCESS
}).catch((error) => {
    // ERROR
});
```

## Demo app

```shell
npm run build
npm run demo
```


