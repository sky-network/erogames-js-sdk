import {ApiService} from "./apiService";
import {Whitelabel} from "./types/whitelabel";
import {TrackingDataModel} from "./types/trackingDataModel";
import {Utils} from "./utils";
import {WhitelabelCache} from "./types/whitelabelCache";
import {InMemoryStorage} from "./inMemoryStorage";

export class Repository {

    private static readonly WL_CACHE_KEY = "storage_wl_cache_key";

    private readonly whitelabelId: string;
    private readonly accessKey = 'b63cd11ff4c45e9b20b454f5dd9a93f6';
    private readonly apiService: ApiService;
    private readonly inMemoryStorage: InMemoryStorage;

    constructor(whitelabelId: string, apiService: ApiService, inMemoryStorage: InMemoryStorage) {
        this.whitelabelId = whitelabelId;
        this.apiService = apiService;
        this.inMemoryStorage = inMemoryStorage;
    }

    async sendEvent(trackingDataModel: TrackingDataModel): Promise<void> {
        const whitelabel = await this.loadWhitelabel();
        const wlTrackingUrl: string = Repository.buildWLTrackingUrl(whitelabel);
        return this.apiService.sendEvent(wlTrackingUrl, trackingDataModel)
    }

    private async loadWhitelabel(useCache: Boolean = true): Promise<Whitelabel> {
        if (useCache) {
            const wl = this.getWhitelabelCache();
            if (wl != null && wl.whitelabel.slug == this.whitelabelId && Utils.isWlCacheValid(wl)) {
                console.log("The whitelabel is taken from the cache.")
                return wl.whitelabel
            }
        }
        const jwtToken = await this.apiService.loadJwtToken(this.accessKey);
        const whitelabels = await this.apiService.getWhitelabels(jwtToken);
        let wl: Whitelabel | undefined = whitelabels.find(element => element.slug === this.whitelabelId);
        if (!wl) {
            throw (`The whitelabel ${this.whitelabelId} does not exist.`)
        }

        const wlCache: WhitelabelCache = {
            whitelabel: wl,
            createdAt: Math.floor(Date.now() / 1000)
        };
        this.inMemoryStorage.setItem(Repository.WL_CACHE_KEY, JSON.stringify(wlCache));
        return wl
    }

    private getWhitelabelCache(): WhitelabelCache | null {
        const wlCacheStr = this.inMemoryStorage.getItem(Repository.WL_CACHE_KEY);
        if (wlCacheStr) return JSON.parse(wlCacheStr);
        return null;
    }

    private static buildWLTrackingUrl(whitelabel: Whitelabel): string {
        if (whitelabel.url.endsWith("/")) {
            return `${whitelabel.url}${ApiService.trackEndpoint}`;
        }
        return `${whitelabel.url}/${ApiService.trackEndpoint}`;
    }
}