export interface AppConfig {
    clientId: string;
    whitelabelId: string;
}