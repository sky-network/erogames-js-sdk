export interface TrackingDataModel {
    type: string;
    category: string;
    client_id: string;
    tracking_uuid: string;
    source_slug: string;
    details: object;
}