import {Whitelabel} from "./whitelabel";

export interface WhitelabelCache {
    whitelabel: Whitelabel;
    createdAt: number;
}