import {Repository} from "./repository"
import {ApiService} from "./apiService"
import {InMemoryStorage} from "./inMemoryStorage"
import {AppConfig} from "./types/appConfig"
import {Utils} from "./utils";
import {TrackingDataModel} from "./types/trackingDataModel";

export class ErogamesAnalytics {
    private static instance: ErogamesAnalytics;

    private config?: AppConfig;
    private repository?: Repository;
    private readonly reservedPrefixes: Array<string> = ["ea_"];

    static getInstance(): ErogamesAnalytics {
        if (!ErogamesAnalytics.instance) ErogamesAnalytics.instance = new ErogamesAnalytics();
        return ErogamesAnalytics.instance;
    }

    /**
     * Erogames Analytics Initialization
     * @param config
     */
    async init(config: AppConfig) {
        if (!config.hasOwnProperty('clientId')) config.clientId = "";
        if (!config.hasOwnProperty('whitelabelId')) config.whitelabelId = ErogamesAnalytics.parseWhitelabelId("erogames");
        this.config = config;

        this.repository = new Repository(config.whitelabelId, new ApiService(), new InMemoryStorage());
    }

    /**
     * Send an event to the server asynchronously.
     *
     * @param event
     * @param params
     */
    async logEvent(event: string, params: Map<string, string> = new Map()): Promise<void> {
        if (!params) params = new Map();
        let trackingUUID = Utils.uuid();
        let nonReservedParams: object = this.extractNonReservedParams(params);
        let trackingDataModel: TrackingDataModel = {
            type: event,
            category: ErogamesAnalytics.getCategory(params),
            client_id: this.config?.clientId!!,
            tracking_uuid: ErogamesAnalytics.getOrCreateTrackId(trackingUUID),
            source_slug: ErogamesAnalytics.getSourceId(params),
            details: nonReservedParams
        }

        await this.repository?.sendEvent(trackingDataModel);
    }

    private extractNonReservedParams(params: Map<string, string>): object {
        let newMap: Map<string, string> = new Map<string, string>();
        params.forEach((value: string, key: string) => {
            let isKeyValid: boolean = true;
            for (const prefix of this.reservedPrefixes) {
                if (key.startsWith(prefix)) isKeyValid = false;
            }
            if (isKeyValid) newMap.set(key, value);
        });

        const mapObj: any = {}
        newMap.forEach((value: string, key: string) => {
            mapObj[key] = value;
        });
        return mapObj;
    }

    private static getCategory(params: Map<string, string>): string {
        if (!params.has("ea_category")) return "unknown";
        return params.get("ea_category") as string;
    }

    private static getOrCreateTrackId(defaultUuid: string): string {
        let cname = "ea:track_id";
        let trackId = Utils.getCookie(cname);
        if (trackId == "") {
            trackId = defaultUuid;
            Utils.setCookie(cname, defaultUuid, 365);
        }

        return trackId;
    }

    private static getSourceId(params: Map<string, string>): string {
        if (params.has("ea_source_id")) return params.get("ea_source_id") as string;
        return Utils.parseQueryString(window.location.href, "ea_source_id", "unknown");
    }

    private static parseWhitelabelId(defaultValue: string): string {
        const url = window.location.href;
        return Utils.parseQueryString(url, "whitelabel", defaultValue);
    }
}