import {WhitelabelCache} from "./types/whitelabelCache";

export class Utils {

    private constructor() {
    }

    static isResponseSuccessful(response: Response): boolean {
        return (response.status >= 200 && response.status <= 299);
    }

    static extractErrorMessage(rawMsg: any): string | null {
        try {
            if (rawMsg["message"]) return rawMsg["message"];
            if (rawMsg["error_description"]) return rawMsg["error_description"];
            if (rawMsg["errors"]) return rawMsg["errors"].join(",");
            if (rawMsg["messages"]) return rawMsg["messages"].join(",");
        } catch (e) {
            console.log(e);
            return null;
        }
        return null;
    }

    static parseQueryString(url: string, key: string, defaultValue: string) {
        let queryParams: Map<string, string> = Utils.parseQueryStrings(url);
        return queryParams.has(key) ? queryParams.get(key) as string : defaultValue;
    }

    static parseQueryStrings(urlStr: string): Map<string, string> {
        const url = new URL(urlStr);
        const queryParams = new URLSearchParams(url.search);
        const keysValues = new Map<string, string>();
        queryParams.forEach((value: string, key: string) => {
            keysValues.set(key, value);
        });
        return keysValues;
    }

    static isWlCacheValid(wlCache: WhitelabelCache) {
        const currentTimeStamp = Math.floor(Date.now() / 1000);
        return (wlCache.createdAt + (24 * 60 * 60)) > currentTimeStamp
    }

    static uuid(): string {
        let time = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = (time + Math.random() * 16) % 16 | 0;
            time = Math.floor(time / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    static getCookie(cname: string): string {
        const name = cname + "=";
        const ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    static setCookie(cname: string, cvalue: string, exdays: number) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        const expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}