import {Whitelabel} from "./types/whitelabel";
import {TrackingDataModel} from "./types/trackingDataModel";
import {Utils} from "./utils";

export class ApiService {

    private readonly _baseUrl = 'https://erogames.com/';
    private readonly _jwtEndpoint = 'api/v1/authenticate';
    private readonly _whitelabelsEndpoint = 'api/v1/whitelabels';
    static readonly trackEndpoint = 'api/v1/tracking';

    private readonly _jsonHeaders: any = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    async loadJwtToken(accessKey: string): Promise<string> {
        const bodyData = {
            "access_key": accessKey
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(this._baseUrl + this._jwtEndpoint, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData['token'] as string;
    }

    async getWhitelabels(jwtToken: string): Promise<Array<Whitelabel>> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = jwtToken;
        const settings = {
            method: 'GET',
            headers: headers,
        };
        const response = await fetch(this._baseUrl + this._whitelabelsEndpoint, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData['whitelabels'] as Array<Whitelabel>;
    }

    async sendEvent(url: string, dataModel: TrackingDataModel): Promise<void> {
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(dataModel)
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
    }
}