export class InMemoryStorage {

    private readonly data: Map<string, string> = new Map();

    setItem(key: string, value: string): void {
        this.data.set(key, value);
    }

    getItem(key: string): string | null {
        const item = this.data.get(key);
        if (item === undefined) return null;
        return item;
    }

    removeItem(key: string): void {
        this.data.delete(key);
    }

    clear(): void {
        this.data.clear();
    }
}