#!/bin/bash
cd "$(dirname $(dirname "$0"))" || exit
mkdir -p demo/public/dist
cp dist/index.umd.js demo/public/dist/index.umd.js
cp dist/index.esm.js demo/public/dist/index.esm.js

cp dist/index.umd.js.map demo/public/dist/index.umd.js.map
cp dist/index.esm.js.map demo/public/dist/index.esm.js.map

cd demo || exit
node app.js
