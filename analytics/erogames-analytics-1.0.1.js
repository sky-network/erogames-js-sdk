(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.erogamesAnalytics = {}));
}(this, (function (exports) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    var Utils = /** @class */ (function () {
        function Utils() {
        }
        Utils.isResponseSuccessful = function (response) {
            return (response.status >= 200 && response.status <= 299);
        };
        Utils.extractErrorMessage = function (rawMsg) {
            try {
                if (rawMsg["message"])
                    return rawMsg["message"];
                if (rawMsg["error_description"])
                    return rawMsg["error_description"];
                if (rawMsg["errors"])
                    return rawMsg["errors"].join(",");
                if (rawMsg["messages"])
                    return rawMsg["messages"].join(",");
            }
            catch (e) {
                console.log(e);
                return null;
            }
            return null;
        };
        Utils.parseQueryString = function (url, key, defaultValue) {
            var queryParams = Utils.parseQueryStrings(url);
            return queryParams.has(key) ? queryParams.get(key) : defaultValue;
        };
        Utils.parseQueryStrings = function (urlStr) {
            var url = new URL(urlStr);
            var queryParams = new URLSearchParams(url.search);
            var keysValues = new Map();
            queryParams.forEach(function (value, key) {
                keysValues.set(key, value);
            });
            return keysValues;
        };
        Utils.isWlCacheValid = function (wlCache) {
            var currentTimeStamp = Math.floor(Date.now() / 1000);
            return (wlCache.createdAt + (24 * 60 * 60)) > currentTimeStamp;
        };
        Utils.uuid = function () {
            var time = new Date().getTime();
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (time + Math.random() * 16) % 16 | 0;
                time = Math.floor(time / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
        };
        Utils.getCookie = function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        };
        Utils.setCookie = function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        };
        return Utils;
    }());

    var ApiService = /** @class */ (function () {
        function ApiService() {
            this._baseUrl = 'https://erogames.com/';
            this._jwtEndpoint = 'api/v1/authenticate';
            this._whitelabelsEndpoint = 'api/v1/whitelabels';
            this._jsonHeaders = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
        }
        ApiService.prototype.loadJwtToken = function (accessKey) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "access_key": accessKey
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(this._baseUrl + this._jwtEndpoint, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData['token']];
                    }
                });
            });
        };
        ApiService.prototype.getWhitelabels = function (jwtToken) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = jwtToken;
                            settings = {
                                method: 'GET',
                                headers: headers,
                            };
                            return [4 /*yield*/, fetch(this._baseUrl + this._whitelabelsEndpoint, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData['whitelabels']];
                    }
                });
            });
        };
        ApiService.prototype.sendEvent = function (url, dataModel) {
            return __awaiter(this, void 0, void 0, function () {
                var settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(dataModel)
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/];
                    }
                });
            });
        };
        ApiService.trackEndpoint = 'api/v1/tracking';
        return ApiService;
    }());

    var Repository = /** @class */ (function () {
        function Repository(whitelabelId, apiService, inMemoryStorage) {
            this.accessKey = 'b63cd11ff4c45e9b20b454f5dd9a93f6';
            this.whitelabelId = whitelabelId;
            this.apiService = apiService;
            this.inMemoryStorage = inMemoryStorage;
        }
        Repository.prototype.sendEvent = function (trackingDataModel) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabel, wlTrackingUrl;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.loadWhitelabel()];
                        case 1:
                            whitelabel = _a.sent();
                            wlTrackingUrl = Repository.buildWLTrackingUrl(whitelabel);
                            return [2 /*return*/, this.apiService.sendEvent(wlTrackingUrl, trackingDataModel)];
                    }
                });
            });
        };
        Repository.prototype.loadWhitelabel = function (useCache) {
            if (useCache === void 0) { useCache = true; }
            return __awaiter(this, void 0, void 0, function () {
                var wl_1, jwtToken, whitelabels, wl, wlCache;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (useCache) {
                                wl_1 = this.getWhitelabelCache();
                                if (wl_1 != null && wl_1.whitelabel.slug == this.whitelabelId && Utils.isWlCacheValid(wl_1)) {
                                    console.log("The whitelabel is taken from the cache.");
                                    return [2 /*return*/, wl_1.whitelabel];
                                }
                            }
                            return [4 /*yield*/, this.apiService.loadJwtToken(this.accessKey)];
                        case 1:
                            jwtToken = _a.sent();
                            return [4 /*yield*/, this.apiService.getWhitelabels(jwtToken)];
                        case 2:
                            whitelabels = _a.sent();
                            wl = whitelabels.find(function (element) { return element.slug === _this.whitelabelId; });
                            if (!wl) {
                                throw ("The whitelabel " + this.whitelabelId + " does not exist.");
                            }
                            wlCache = {
                                whitelabel: wl,
                                createdAt: Math.floor(Date.now() / 1000)
                            };
                            this.inMemoryStorage.setItem(Repository.WL_CACHE_KEY, JSON.stringify(wlCache));
                            return [2 /*return*/, wl];
                    }
                });
            });
        };
        Repository.prototype.getWhitelabelCache = function () {
            var wlCacheStr = this.inMemoryStorage.getItem(Repository.WL_CACHE_KEY);
            if (wlCacheStr)
                return JSON.parse(wlCacheStr);
            return null;
        };
        Repository.buildWLTrackingUrl = function (whitelabel) {
            if (whitelabel.url.endsWith("/")) {
                return "" + whitelabel.url + ApiService.trackEndpoint;
            }
            return whitelabel.url + "/" + ApiService.trackEndpoint;
        };
        Repository.WL_CACHE_KEY = "storage_wl_cache_key";
        return Repository;
    }());

    var InMemoryStorage = /** @class */ (function () {
        function InMemoryStorage() {
            this.data = new Map();
        }
        InMemoryStorage.prototype.setItem = function (key, value) {
            this.data.set(key, value);
        };
        InMemoryStorage.prototype.getItem = function (key) {
            var item = this.data.get(key);
            if (item === undefined)
                return null;
            return item;
        };
        InMemoryStorage.prototype.removeItem = function (key) {
            this.data.delete(key);
        };
        InMemoryStorage.prototype.clear = function () {
            this.data.clear();
        };
        return InMemoryStorage;
    }());

    var ErogamesAnalytics = /** @class */ (function () {
        function ErogamesAnalytics() {
            this.reservedPrefixes = ["ea_"];
        }
        ErogamesAnalytics.getInstance = function () {
            if (!ErogamesAnalytics.instance)
                ErogamesAnalytics.instance = new ErogamesAnalytics();
            return ErogamesAnalytics.instance;
        };
        /**
         * Erogames Analytics Initialization
         * @param config
         */
        ErogamesAnalytics.prototype.init = function (config) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (!config.hasOwnProperty('clientId'))
                        config.clientId = "";
                    if (!config.hasOwnProperty('whitelabelId'))
                        config.whitelabelId = ErogamesAnalytics.parseWhitelabelId("erogames");
                    this.config = config;
                    this.repository = new Repository(config.whitelabelId, new ApiService(), new InMemoryStorage());
                    return [2 /*return*/];
                });
            });
        };
        /**
         * Send an event to the server asynchronously.
         *
         * @param event
         * @param params
         */
        ErogamesAnalytics.prototype.logEvent = function (event, params) {
            var _a, _b;
            if (params === void 0) { params = new Map(); }
            return __awaiter(this, void 0, void 0, function () {
                var trackingUUID, nonReservedParams, trackingDataModel;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!params)
                                params = new Map();
                            trackingUUID = Utils.uuid();
                            nonReservedParams = this.extractNonReservedParams(params);
                            trackingDataModel = {
                                type: event,
                                category: ErogamesAnalytics.getCategory(params),
                                client_id: (_a = this.config) === null || _a === void 0 ? void 0 : _a.clientId,
                                tracking_uuid: ErogamesAnalytics.getOrCreateTrackId(trackingUUID),
                                source_slug: ErogamesAnalytics.getSourceId(params),
                                details: nonReservedParams
                            };
                            return [4 /*yield*/, ((_b = this.repository) === null || _b === void 0 ? void 0 : _b.sendEvent(trackingDataModel))];
                        case 1:
                            _c.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        ErogamesAnalytics.prototype.extractNonReservedParams = function (params) {
            var _this = this;
            var newMap = new Map();
            params.forEach(function (value, key) {
                var isKeyValid = true;
                for (var _i = 0, _a = _this.reservedPrefixes; _i < _a.length; _i++) {
                    var prefix = _a[_i];
                    if (key.startsWith(prefix))
                        isKeyValid = false;
                }
                if (isKeyValid)
                    newMap.set(key, value);
            });
            var mapObj = {};
            newMap.forEach(function (value, key) {
                mapObj[key] = value;
            });
            return mapObj;
        };
        ErogamesAnalytics.getCategory = function (params) {
            if (!params.has("ea_category"))
                return "unknown";
            return params.get("ea_category");
        };
        ErogamesAnalytics.getOrCreateTrackId = function (defaultUuid) {
            var cname = "ea:track_id";
            var trackId = Utils.getCookie(cname);
            if (trackId == "") {
                trackId = defaultUuid;
                Utils.setCookie(cname, defaultUuid, 365);
            }
            return trackId;
        };
        ErogamesAnalytics.getSourceId = function (params) {
            if (params.has("ea_source_id"))
                return params.get("ea_source_id");
            return Utils.parseQueryString(window.location.href, "ea_source_id", "unknown");
        };
        ErogamesAnalytics.parseWhitelabelId = function (defaultValue) {
            var url = window.location.href;
            return Utils.parseQueryString(url, "whitelabel", defaultValue);
        };
        return ErogamesAnalytics;
    }());

    function analytics() {
        return ErogamesAnalytics.getInstance();
    }

    exports.analytics = analytics;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
