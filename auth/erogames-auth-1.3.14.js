(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.erogames = {}));
}(this, (function (exports) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    var Utils = /** @class */ (function () {
        function Utils() {
        }
        Utils.buildAuthUrl = function (authorizeUrl, codeChallenge, redirectUri, clientId, locale, forceRegistration) {
            var delimiter = (authorizeUrl.indexOf('?') !== -1) ? '&' : '?';
            return "" + authorizeUrl + delimiter +
                ("code_challenge=" + codeChallenge) +
                ("&redirect_uri=" + redirectUri) +
                ("&client_id=" + clientId) +
                ("&locale=" + locale) +
                ("&force_registration=" + forceRegistration) +
                "&code_challenge_method=S256" +
                "&response_type=code" +
                "&disclaimer=none";
        };
        Utils.getQueryValue = function (locationSearch, queryParam, defaultValue) {
            queryParam = queryParam.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + queryParam + '=([^&#]*)');
            var results = regex.exec(locationSearch);
            return results === null ? defaultValue : decodeURIComponent(results[1].replace(/\+/g, ' '));
        };
        Utils.buildLogoutUrl = function (url, token, redirectUri) {
            return url + "/logout?token=" + token + "&redirect_uri=" + redirectUri;
        };
        Utils.isResponseSuccessful = function (response) {
            return (response.status >= 200 && response.status <= 299);
        };
        Utils.getCodeFromIframe = function (url) {
            var codeValue = null;
            var iframe = document.createElement("iframe");
            iframe.name = this.iframeWindowName;
            iframe.style.display = "none";
            iframe.onload = function () {
                var _a, _b, _c;
                try {
                    var queryParams = (_a = iframe.contentWindow) === null || _a === void 0 ? void 0 : _a.location.search;
                    codeValue = Utils.getQueryValue(queryParams, "code", null);
                }
                catch (e) {
                    console.log(e);
                }
                var payload = { value: codeValue, origin: url, is_erogames_js_sdk: true };
                (_b = iframe.contentWindow) === null || _b === void 0 ? void 0 : _b.parent.postMessage(JSON.stringify(payload), window.origin);
                (_c = iframe.parentNode) === null || _c === void 0 ? void 0 : _c.removeChild(iframe);
            };
            iframe.src = url;
            document.body.appendChild(iframe);
        };
        Utils.delay = function (ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        };
        Utils.extractErrorMessage = function (rawMsg) {
            try {
                if (rawMsg["message"]) {
                    return rawMsg["message"];
                }
                if (rawMsg["error_description"]) {
                    return rawMsg["error_description"];
                }
                if (rawMsg["errors"]) {
                    return rawMsg["errors"].join(",");
                }
            }
            catch (e) {
                console.log(e);
                return null;
            }
            return null;
        };
        Utils.removeQueryString = function (uri, param) {
            if (!uri)
                return uri;
            if (!Utils.isUrlValid(uri))
                return uri;
            var url = new URL(uri);
            var regex = new RegExp('[?&]' + param + '=[^&]+');
            var newSearch = url.search.replace(regex, '').replace(/^&/, '?');
            return url.protocol + "//" + url.host + url.pathname + newSearch;
        };
        Utils.isUrlValid = function (url) {
            try {
                new URL(url);
            }
            catch (e) {
                console.log("Invalid URL: " + url);
                return false;
            }
            return true;
        };
        Utils.defaultLang = 'en';
        Utils.popupWindowName = 'ErogamesOAuth';
        Utils.iframeWindowName = 'com_erogames_sdk_auth_iframe';
        Utils.supportedLanguages = ['en', 'fr', 'zh', 'ja', 'es'];
        return Utils;
    }());

    var Repository = /** @class */ (function () {
        function Repository(clientId, apiService, inMemoryStorage, localStorage) {
            this.clientId = clientId;
            this.apiService = apiService;
            this.inMemoryStorage = inMemoryStorage;
            this.localStorage = localStorage;
        }
        Repository.prototype.loadJwtToken = function (accessKey) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.apiService.loadJwtToken(accessKey)];
                });
            });
        };
        Repository.prototype.setWhitelabel = function (whitelabel) {
            this.localStorage.setWhitelabel(whitelabel);
        };
        Repository.prototype.getWhitelabel = function () {
            return this.localStorage.getWhitelabel();
        };
        Repository.prototype.loadWhitelabel = function (jwtToken, whitelabelId) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabels, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.loadWhitelabels(jwtToken)];
                        case 1:
                            whitelabels = _a.sent();
                            if (whitelabelId) {
                                whitelabel = whitelabels.find(function (element) { return element.slug === whitelabelId; });
                                if (!whitelabel) {
                                    throw "The whitelabel " + whitelabelId + " does not exist.";
                                }
                                console.log('current wl: ' + whitelabel.slug);
                                this.setWhitelabel(whitelabel);
                                return [2 /*return*/, whitelabel];
                            }
                            throw "Invalid whitelabel " + whitelabelId + ".";
                    }
                });
            });
        };
        Repository.prototype.loadWhitelabels = function (jwtToken) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.apiService.getWhitelabels(jwtToken)];
                });
            });
        };
        Repository.prototype.loadToken = function (code, grantType, redirectUri, codeVerifier) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabel, token;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            whitelabel = this.getWhitelabel();
                            if (!whitelabel)
                                throw 'The whitelabel can\'t be null.';
                            return [4 /*yield*/, this.apiService.getToken(whitelabel.token_url, this.clientId, code, grantType, redirectUri, codeVerifier)];
                        case 1:
                            token = _a.sent();
                            this.setToken(token);
                            return [2 /*return*/, token];
                    }
                });
            });
        };
        Repository.prototype.loadUser = function (profileUrl) {
            if (profileUrl === void 0) { profileUrl = null; }
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel, user;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            if (!token)
                                throw ("The token can't be null.");
                            whitelabel = null;
                            if (!profileUrl) {
                                whitelabel = this.getWhitelabel();
                                if (!whitelabel)
                                    throw ("The whitelabel can't be null.");
                            }
                            if (!profileUrl) {
                                profileUrl = whitelabel.profile_url;
                            }
                            return [4 /*yield*/, this.apiService.getUser(profileUrl, "Bearer " + token.access_token)];
                        case 2:
                            user = _a.sent();
                            this.setUser(user);
                            return [2 /*return*/, user];
                    }
                });
            });
        };
        Repository.prototype.reloadUser = function () {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel, user;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            whitelabel = this.getWhitelabel();
                            if (!token || !whitelabel)
                                throw 'The token and/or whitelabel can\'t be null.';
                            return [4 /*yield*/, this.apiService.getUser(whitelabel.profile_url, "Bearer " + token.access_token)];
                        case 2:
                            user = _a.sent();
                            this.setUser(user);
                            return [2 /*return*/, user];
                    }
                });
            });
        };
        Repository.prototype.setToken = function (token) {
            this.inMemoryStorage.setToken(token);
        };
        Repository.prototype.getToken = function () {
            return this.inMemoryStorage.getToken();
        };
        Repository.prototype.setUser = function (user) {
            this.inMemoryStorage.setUser(user);
        };
        Repository.prototype.getUser = function () {
            return this.inMemoryStorage.getUser();
        };
        Repository.prototype.clearLocalData = function () {
            this.removeUser();
            this.removeToken();
            this.removeWhitelabel();
        };
        Repository.prototype.removeUser = function () {
            this.inMemoryStorage.removeUser();
            this.localStorage.removeUser();
        };
        Repository.prototype.removeToken = function () {
            this.inMemoryStorage.removeToken();
            this.localStorage.removeUser();
        };
        Repository.prototype.removeWhitelabel = function () {
            this.inMemoryStorage.removeWhitelabel();
            this.localStorage.removeUser();
        };
        Repository.prototype.refreshToken = function () {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel, refreshedToken;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            token = this.getToken();
                            whitelabel = this.getWhitelabel();
                            if (!token)
                                throw ("The token can't be null.");
                            if (!whitelabel)
                                throw ("The whitelabel can't be null.");
                            return [4 /*yield*/, this.apiService.refreshToken(whitelabel.token_url, this.clientId, token.refresh_token)];
                        case 1:
                            refreshedToken = _a.sent();
                            this.setToken(refreshedToken);
                            return [2 /*return*/, refreshedToken];
                    }
                });
            });
        };
        Repository.prototype.getOrRefreshToken = function () {
            return __awaiter(this, void 0, void 0, function () {
                var token;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getToken()];
                        case 1:
                            token = _a.sent();
                            if (!(token != null && Repository.isTokenExpired(token))) return [3 /*break*/, 3];
                            return [4 /*yield*/, this.refreshToken()];
                        case 2: return [2 /*return*/, _a.sent()];
                        case 3: return [2 /*return*/, token];
                    }
                });
            });
        };
        Repository.prototype.proceedPayment = function (paymentId, amount) {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            whitelabel = this.getWhitelabel();
                            if (!token)
                                throw ("The token can't be null.");
                            if (!whitelabel)
                                throw ("The whitelabel can't be null.");
                            return [4 /*yield*/, this.apiService.proceedPayment(whitelabel.url + "/api/v1/payments", "Bearer " + token.access_token, paymentId, amount)];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        Repository.prototype.addDataPoints = function (dataPoints) {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            whitelabel = this.getWhitelabel();
                            if (!token)
                                throw ("The token can't be null.");
                            if (!whitelabel)
                                throw ("The whitelabel can't be null.");
                            return [4 /*yield*/, this.apiService.addDataPoints(whitelabel.url + "/api/v1/me/data_point_collection", "Bearer " + token.access_token, dataPoints)];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        Repository.prototype.getCurrentQuestData = function () {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            whitelabel = this.getWhitelabel();
                            if (!token)
                                throw ("The token can't be null.");
                            if (!whitelabel)
                                throw ("The whitelabel can't be null.");
                            return [4 /*yield*/, this.apiService.getCurrentQuestData(whitelabel.url + "/api/v1/quests/current", "Bearer " + token.access_token)];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        Repository.prototype.getPaymentInfo = function (paymentId) {
            return __awaiter(this, void 0, void 0, function () {
                var token, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.getOrRefreshToken()];
                        case 1:
                            token = _a.sent();
                            whitelabel = this.getWhitelabel();
                            if (!token)
                                throw ("The token can't be null.");
                            if (!whitelabel)
                                throw ("The whitelabel can't be null.");
                            return [4 /*yield*/, this.apiService.getPaymentInfo(whitelabel.url + "/api/v1/payments/" + paymentId, "Bearer " + token.access_token)];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        Repository.prototype.setLocale = function (lang) {
            var l = (Utils.supportedLanguages.indexOf(lang) > -1)
                ? lang : Utils.defaultLang;
            this.localStorage.setLocale(l);
        };
        Repository.prototype.getLocale = function () {
            var l = this.localStorage.getLocale();
            if (l)
                return l;
            return Utils.defaultLang;
        };
        Repository.prototype.loadTokenByPassword = function (tokenUrl, username, password) {
            return __awaiter(this, void 0, void 0, function () {
                var token;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.apiService.getTokenByPassword(tokenUrl, this.clientId, username, password)];
                        case 1:
                            token = _a.sent();
                            this.setToken(token);
                            return [2 /*return*/, token];
                    }
                });
            });
        };
        Repository.prototype.registerUser = function (baseUrl, clientSecret, username, password, email, checkTermsOfUse) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.apiService.registerUser(baseUrl, this.clientId, clientSecret, username, password, email, checkTermsOfUse)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        Repository.isTokenExpired = function (token) {
            var timestamp = Date.now() / 1000;
            return (timestamp + 10) > (token.created_at + token.expires_in);
        };
        return Repository;
    }());

    var ApiService = /** @class */ (function () {
        function ApiService() {
            this._baseUrl = 'https://erogames.com/';
            this._jwtEndpoint = 'api/v1/authenticate';
            this._whitelabelsEndpoint = 'api/v1/whitelabels';
            this._registerUserEndpoint = 'api/v1/register';
            this._jsonHeaders = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
        }
        ApiService.prototype.loadJwtToken = function (accessKey) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "access_key": accessKey
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(this._baseUrl + this._jwtEndpoint, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData['token']];
                    }
                });
            });
        };
        ApiService.prototype.getWhitelabels = function (jwtToken) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = jwtToken;
                            settings = {
                                method: 'GET',
                                headers: headers,
                            };
                            return [4 /*yield*/, fetch(this._baseUrl + this._whitelabelsEndpoint, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData['whitelabels']];
                    }
                });
            });
        };
        ApiService.prototype.getUser = function (profileUrl, accessToken) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = accessToken;
                            settings = {
                                method: 'GET',
                                headers: headers,
                            };
                            return [4 /*yield*/, fetch(profileUrl, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData['user']];
                    }
                });
            });
        };
        ApiService.prototype.getToken = function (url, clientId, code, grantType, redirectUri, codeVerifier) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "client_id": clientId,
                                "code": code,
                                "grant_type": grantType,
                                "redirect_uri": redirectUri,
                                "code_verifier": codeVerifier
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData];
                    }
                });
            });
        };
        ApiService.prototype.refreshToken = function (tokenUrl, clientId, refreshToken) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "client_id": clientId,
                                "grant_type": 'refresh_token',
                                "refresh_token": refreshToken,
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(tokenUrl, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData];
                    }
                });
            });
        };
        ApiService.prototype.proceedPayment = function (url, token, paymentId, amount) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = token;
                            bodyData = {
                                "paymentId": paymentId,
                                "amount": amount,
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, true];
                    }
                });
            });
        };
        ApiService.prototype.addDataPoints = function (url, token, dataPoints) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = token;
                            bodyData = {
                                data_points: dataPoints
                            };
                            settings = {
                                method: 'PUT',
                                headers: headers,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, true];
                    }
                });
            });
        };
        ApiService.prototype.getCurrentQuestData = function (url, token) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = token;
                            settings = {
                                method: 'GET',
                                headers: headers,
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData];
                    }
                });
            });
        };
        ApiService.prototype.getPaymentInfo = function (url, token) {
            return __awaiter(this, void 0, void 0, function () {
                var headers, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            headers = this._jsonHeaders;
                            headers['Authorization'] = token;
                            settings = {
                                method: 'GET',
                                headers: headers,
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData];
                    }
                });
            });
        };
        ApiService.prototype.getTokenByPassword = function (url, clientId, username, password) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "client_id": clientId,
                                "grant_type": "password",
                                "login": username,
                                "password": password,
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            return [4 /*yield*/, fetch(url, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/, responseData];
                    }
                });
            });
        };
        ApiService.prototype.registerUser = function (baseUrl, clientId, clientSecret, username, password, email, checkTermsOfUse) {
            return __awaiter(this, void 0, void 0, function () {
                var bodyData, settings, response, responseData;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            bodyData = {
                                "client_id": clientId,
                                "client_secret": clientSecret,
                                "username": username,
                                "password": password,
                                "passwordConfirmation": password,
                                "email": email,
                                "checkTermsOfUse": checkTermsOfUse,
                            };
                            settings = {
                                method: 'POST',
                                headers: this._jsonHeaders,
                                body: JSON.stringify(bodyData)
                            };
                            if (!baseUrl.endsWith("/"))
                                baseUrl = baseUrl + "/";
                            return [4 /*yield*/, fetch(baseUrl + this._registerUserEndpoint, settings)];
                        case 1:
                            response = _a.sent();
                            return [4 /*yield*/, response.json()];
                        case 2:
                            responseData = _a.sent();
                            if (!Utils.isResponseSuccessful(response))
                                throw Utils.extractErrorMessage(responseData);
                            return [2 /*return*/];
                    }
                });
            });
        };
        return ApiService;
    }());

    var InMemoryStorage = /** @class */ (function () {
        function InMemoryStorage() {
            this.locale = null;
            this.token = null;
            this.user = null;
            this.whitelabel = null;
        }
        InMemoryStorage.prototype.setLocale = function (lang) {
            this.locale = lang;
        };
        InMemoryStorage.prototype.getLocale = function () {
            return this.locale;
        };
        InMemoryStorage.prototype.setToken = function (token) {
            this.token = token;
        };
        InMemoryStorage.prototype.getToken = function () {
            return this.token;
        };
        InMemoryStorage.prototype.removeToken = function () {
            this.token = null;
        };
        InMemoryStorage.prototype.setUser = function (user) {
            this.user = user;
        };
        InMemoryStorage.prototype.getUser = function () {
            return this.user;
        };
        InMemoryStorage.prototype.removeUser = function () {
            this.user = null;
        };
        InMemoryStorage.prototype.setWhitelabel = function (whitelabel) {
            this.whitelabel = whitelabel;
        };
        InMemoryStorage.prototype.getWhitelabel = function () {
            return this.whitelabel;
        };
        InMemoryStorage.prototype.removeWhitelabel = function () {
            this.whitelabel = null;
        };
        return InMemoryStorage;
    }());

    var PrefUtil = /** @class */ (function () {
        function PrefUtil() {
        }
        PrefUtil.setOnLogin = function (onLogin) {
            window.localStorage.setItem(PrefUtil.storageOnLoginKey, onLogin ? "1" : "0");
        };
        PrefUtil.isOnLogin = function () {
            var isOnLoginStr = window.localStorage.getItem(PrefUtil.storageOnLoginKey);
            var isOnLoginInt = parseInt(isOnLoginStr !== null && isOnLoginStr !== void 0 ? isOnLoginStr : "");
            if (isOnLoginInt) {
                return isOnLoginInt > 0;
            }
            return false;
        };
        PrefUtil.storageOnLoginKey = "ero_sdk_pref_on_login";
        return PrefUtil;
    }());

    var AuthUtil = /** @class */ (function () {
        function AuthUtil() {
        }
        AuthUtil.buildRedirectUri = function (encode) {
            if (encode === void 0) { encode = false; }
            var url = window.location.href;
            url = Utils.removeQueryString(url, "code");
            if (encode)
                url = encodeURIComponent(url);
            return url;
        };
        return AuthUtil;
    }());

    var LocalStorage = /** @class */ (function () {
        function LocalStorage() {
            this.STORAGE_WHITELABEL_KEY = "ero_sdk_auth_storage_whitelabel_key";
            this.STORAGE_TOKEN_KEY = "ero_sdk_auth_storage_token_key";
            this.STORAGE_USER_KEY = "ero_sdk_auth_storage_user_key";
            this.STORAGE_LANG_KEY = "ero_sdk_auth_storage_key";
        }
        LocalStorage.prototype.setWhitelabel = function (whitelabel) {
            window.localStorage.setItem(this.STORAGE_WHITELABEL_KEY, JSON.stringify(whitelabel));
        };
        LocalStorage.prototype.getWhitelabel = function () {
            return JSON.parse(window.localStorage.getItem(this.STORAGE_WHITELABEL_KEY));
        };
        LocalStorage.prototype.setToken = function (token) {
            window.localStorage.setItem(this.STORAGE_TOKEN_KEY, JSON.stringify(token));
        };
        LocalStorage.prototype.getToken = function () {
            return JSON.parse(window.localStorage.getItem(this.STORAGE_TOKEN_KEY));
        };
        LocalStorage.prototype.setUser = function (user) {
            window.localStorage.setItem(this.STORAGE_USER_KEY, JSON.stringify(user));
        };
        LocalStorage.prototype.getUser = function () {
            return JSON.parse(window.localStorage.getItem(this.STORAGE_USER_KEY));
        };
        LocalStorage.prototype.removeUser = function () {
            window.localStorage.removeItem(this.STORAGE_USER_KEY);
        };
        LocalStorage.prototype.removeToken = function () {
            window.localStorage.removeItem(this.STORAGE_TOKEN_KEY);
        };
        LocalStorage.prototype.removeWhitelabel = function () {
            window.localStorage.removeItem(this.STORAGE_WHITELABEL_KEY);
        };
        LocalStorage.prototype.setLocale = function (lang) {
            window.localStorage.setItem(this.STORAGE_LANG_KEY, lang);
        };
        LocalStorage.prototype.getLocale = function () {
            return window.localStorage.getItem(this.STORAGE_LANG_KEY);
        };
        return LocalStorage;
    }());

    var Pkce = /** @class */ (function () {
        function Pkce() {
        }
        Pkce.getCodeVerifier = function (generateNew) {
            if (generateNew === void 0) { generateNew = false; }
            var codeVerifier = window.localStorage.getItem(Pkce.CODE_VERIFIER_KEY);
            if (codeVerifier == null || generateNew) {
                codeVerifier = this.generateCodeVerifier();
                window.localStorage.setItem(Pkce.CODE_VERIFIER_KEY, codeVerifier);
            }
            return codeVerifier;
        };
        Pkce.generateCodeVerifier = function () {
            var array = new Uint32Array(28);
            window.crypto.getRandomValues(array);
            return Array.from(array, function (dec) { return ('0' + dec.toString(16)).substr(-2); }).join('');
        };
        Pkce.generateCodeChallenge = function (codeVerifier) {
            return __awaiter(this, void 0, void 0, function () {
                var hashed;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.sha256(codeVerifier)];
                        case 1:
                            hashed = _a.sent();
                            return [2 /*return*/, this.base64URLEncode(hashed)];
                    }
                });
            });
        };
        Pkce.clear = function () {
            window.localStorage.removeItem(Pkce.CODE_VERIFIER_KEY);
        };
        Pkce.base64URLEncode = function (str) {
            // @ts-ignore
            return btoa(String.fromCharCode.apply(null, new Uint8Array(str)))
                .replace(/\+/g, '-')
                .replace(/\//g, '_')
                .replace(/=+$/, '');
        };
        Pkce.sha256 = function (text) {
            return __awaiter(this, void 0, void 0, function () {
                var encoder, data;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            encoder = new TextEncoder();
                            data = encoder.encode(text);
                            return [4 /*yield*/, window.crypto.subtle.digest('SHA-256', data)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        Pkce.CODE_VERIFIER_KEY = "ero_sdk_auth_storage_code_verifier_key";
        return Pkce;
    }());

    var ErogamesAuth = /** @class */ (function () {
        function ErogamesAuth() {
            this.forceLogin = true;
            this._accessKey = 'b63cd11ff4c45e9b20b454f5dd9a93f6';
        }
        ErogamesAuth.getInstance = function () {
            if (!ErogamesAuth.instance)
                ErogamesAuth.instance = new ErogamesAuth();
            return ErogamesAuth.instance;
        };
        /**
         * Erogames Auth Initialization
         * @param config
         */
        ErogamesAuth.prototype.init = function (config) {
            return __awaiter(this, void 0, void 0, function () {
                var user, codeValue, baseUrl;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!config.hasOwnProperty('clientId'))
                                config.clientId = "";
                            if (!config.hasOwnProperty('autoLogin'))
                                config.autoLogin = true;
                            this.config = config;
                            this.repository = new Repository(this.config.clientId, new ApiService(), new InMemoryStorage(), new LocalStorage());
                            if (window.name === Utils.iframeWindowName)
                                return [2 /*return*/];
                            user = this.getUser();
                            if (!!user) return [3 /*break*/, 5];
                            codeValue = Utils.getQueryValue(window.location.search, "code", null);
                            if (!codeValue) return [3 /*break*/, 2];
                            baseUrl = Utils.removeQueryString(window.location.href, "code");
                            window.history.replaceState(null, "", baseUrl);
                            return [4 /*yield*/, this.proceedAuth(codeValue)];
                        case 1:
                            _a.sent();
                            return [3 /*break*/, 4];
                        case 2: return [4 /*yield*/, this.login(this.repository.getLocale())];
                        case 3:
                            _a.sent();
                            _a.label = 4;
                        case 4: return [3 /*break*/, 6];
                        case 5:
                            this.onAuthSuccess(user);
                            _a.label = 6;
                        case 6:
                            this.forceLogin = this.config.autoLogin;
                            window.onmessage = function (event) { return __awaiter(_this, void 0, void 0, function () {
                                var code, e_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!(window.origin === event.origin)) return [3 /*break*/, 6];
                                            _a.label = 1;
                                        case 1:
                                            _a.trys.push([1, 5, , 6]);
                                            code = JSON.parse(event.data);
                                            if (!(code === null || code === void 0 ? void 0 : code.is_erogames_js_sdk))
                                                return [2 /*return*/];
                                            if (!code.value) return [3 /*break*/, 3];
                                            return [4 /*yield*/, this.proceedAuth(code.value)];
                                        case 2:
                                            _a.sent();
                                            return [3 /*break*/, 4];
                                        case 3:
                                            if (this.forceLogin) {
                                                window.location.replace(code.origin);
                                            }
                                            else {
                                                this.onAuthSuccess(null);
                                            }
                                            this.forceLogin = true;
                                            _a.label = 4;
                                        case 4: return [3 /*break*/, 6];
                                        case 5:
                                            e_1 = _a.sent();
                                            console.log("ErogamesAuth -> onmessage:" + e_1);
                                            console.log("ErogamesAuth -> event data:" + event.data);
                                            return [3 /*break*/, 6];
                                        case 6: return [2 /*return*/];
                                    }
                                });
                            }); };
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * Monitors the authentication state.
         * A callback will trigger once the user has logged in, signed up, or logged out.
         *
         * @param authCallback
         */
        ErogamesAuth.prototype.onAuth = function (authCallback) {
            this.authCallback = authCallback;
            if (this.onAuthSuccessPending != null) {
                this.authCallback(this.onAuthSuccessPending, null);
                this.onAuthSuccessPending = null;
                this.onAuthErrorPending = null;
            }
            if (this.onAuthErrorPending != null) {
                this.authCallback(null, this.onAuthErrorPending);
                this.onAuthSuccessPending = null;
                this.onAuthErrorPending = null;
            }
        };
        /**
         * Starts sign in process. The page may be reloaded.
         *
         * @param lang - the preferable language of the "sign in" web page.
         */
        ErogamesAuth.prototype.login = function (lang) {
            if (lang === void 0) { lang = Utils.defaultLang; }
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.repository.setLocale(lang);
                            return [4 /*yield*/, this.loginInternal(false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * Sign in by username(email)/password.
         *
         * @param username
         * @param password
         */
        ErogamesAuth.prototype.loginByPassword = function (username, password) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabelId, jwtToken, whitelabel, user, e_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.repository.setLocale(Utils.defaultLang);
                            PrefUtil.setOnLogin(true);
                            whitelabelId = ErogamesAuth.getWhitelabelId();
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 6, , 7]);
                            return [4 /*yield*/, this.repository.loadJwtToken(this._accessKey)];
                        case 2:
                            jwtToken = _a.sent();
                            return [4 /*yield*/, this.repository.loadWhitelabel(jwtToken, whitelabelId)];
                        case 3:
                            whitelabel = _a.sent();
                            return [4 /*yield*/, this.repository.loadTokenByPassword(whitelabel.token_url, username, password)];
                        case 4:
                            _a.sent();
                            return [4 /*yield*/, this.repository.loadUser(whitelabel.profile_url)];
                        case 5:
                            user = _a.sent();
                            this.onAuthSuccess(user);
                            return [3 /*break*/, 7];
                        case 6:
                            e_2 = _a.sent();
                            this.onAuthError({ code: ErogamesAuth.authError, message: e_2 });
                            return [3 /*break*/, 7];
                        case 7: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * Starts sign up process. The page may be reloaded.
         *
         * @param lang - the preferable language of the "sign in" web page.
         */
        ErogamesAuth.prototype.signup = function (lang) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.repository.setLocale(Utils.defaultLang);
                            return [4 /*yield*/, this.loginInternal(true)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * Register a new user
         *
         * @param clientSecret
         * @param username
         * @param password
         * @param email
         * @param checkTermsOfUse
         */
        ErogamesAuth.prototype.registerUser = function (clientSecret, username, password, email, checkTermsOfUse) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabelId, jwtToken, whitelabel;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            whitelabelId = ErogamesAuth.getWhitelabelId();
                            return [4 /*yield*/, this.repository.loadJwtToken(this._accessKey)];
                        case 1:
                            jwtToken = _a.sent();
                            return [4 /*yield*/, this.repository.loadWhitelabel(jwtToken, whitelabelId)];
                        case 2:
                            whitelabel = _a.sent();
                            return [4 /*yield*/, this.repository.registerUser(whitelabel.url, clientSecret, username, password, email, checkTermsOfUse)];
                        case 3:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * Clear all local authentication data.
         */
        ErogamesAuth.prototype.logout = function () {
            this.repository.clearLocalData();
            if (this.authCallback)
                this.authCallback(null, null);
        };
        /**
         * Returns the current user data, otherwise null if the user is not logged in.
         */
        ErogamesAuth.prototype.getUser = function () {
            return this.repository.getUser();
        };
        /**
         * Refreshes the current user data.
         */
        ErogamesAuth.prototype.reloadUser = function () {
            return __awaiter(this, void 0, void 0, function () {
                var user;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.repository.reloadUser()];
                        case 1:
                            user = _a.sent();
                            if (this.authCallback)
                                this.authCallback(user, null);
                            return [2 /*return*/, user];
                    }
                });
            });
        };
        /**
         * Returns the current token, otherwise null if the user is not logged in.
         */
        ErogamesAuth.prototype.getToken = function () {
            return this.repository.getToken();
        };
        /**
         * Refreshes the current token. For example, can be used if the token is expired.
         */
        ErogamesAuth.prototype.refreshToken = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, this.repository.refreshToken()];
                });
            });
        };
        /**
         * Returns a whitelabel info.
         */
        ErogamesAuth.prototype.getWhitelabel = function () {
            return this.repository.getWhitelabel();
        };
        /**
         * Load the whitelabel. For example, can be used if the user is not logged in.
         */
        ErogamesAuth.prototype.loadWhitelabel = function () {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabelId, jwtToken;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            whitelabelId = ErogamesAuth.getWhitelabelId();
                            return [4 /*yield*/, this.repository.loadJwtToken(this._accessKey)];
                        case 1:
                            jwtToken = _a.sent();
                            return [4 /*yield*/, this.repository.loadWhitelabel(jwtToken, whitelabelId)];
                        case 2: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Proceed a new payment.
         *
         * @param paymentId
         * @param amount
         */
        ErogamesAuth.prototype.proceedPayment = function (paymentId, amount) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.repository.proceedPayment(paymentId, amount)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Create/update user data points.
         * Sending new data points will create a new entries.
         * Sending a data point that was already created before will update the previous values.
         *
         * @param dataPoints
         */
        ErogamesAuth.prototype.addDataPoints = function (dataPoints) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.repository.addDataPoints(dataPoints)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Loads current quest info.
         * Load key quest statistics and fields such as quest title and
         * description.The data returned also has information about the current ranking of
         * user's clan (user is loaded based on used auth token) and also current user's
         * individual contribution to the clan score.This endpoint allows implementing in-game
         * quest status widgets for more immersive gameplay.
         */
        ErogamesAuth.prototype.loadCurrentQuest = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.repository.getCurrentQuestData()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Loads payment information.
         * @param paymentId
         */
        ErogamesAuth.prototype.loadPaymentInfo = function (paymentId) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.repository.getPaymentInfo(paymentId)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        /**
         * Check if the sign in/up process is going yet.
         */
        ErogamesAuth.prototype.isLoginGoing = function () {
            return PrefUtil.isOnLogin();
        };
        ErogamesAuth.prototype.loginInternal = function (forceRegistration) {
            return __awaiter(this, void 0, void 0, function () {
                var authUrl;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            PrefUtil.setOnLogin(true);
                            return [4 /*yield*/, this.getAuthUrl(forceRegistration)];
                        case 1:
                            authUrl = _a.sent();
                            Utils.getCodeFromIframe(authUrl);
                            return [2 /*return*/];
                    }
                });
            });
        };
        ErogamesAuth.prototype.getAuthUrl = function (forceRegistration) {
            return __awaiter(this, void 0, void 0, function () {
                var whitelabelId, jwtToken, whitelabel, authorizeUrl, codeChallenge, redirectUri, clientId, locale;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            whitelabelId = ErogamesAuth.getWhitelabelId();
                            return [4 /*yield*/, this.repository.loadJwtToken(this._accessKey)];
                        case 1:
                            jwtToken = _a.sent();
                            return [4 /*yield*/, this.repository.loadWhitelabel(jwtToken, whitelabelId)];
                        case 2:
                            whitelabel = _a.sent();
                            authorizeUrl = whitelabel.authorize_url;
                            return [4 /*yield*/, Pkce.generateCodeChallenge(Pkce.getCodeVerifier())];
                        case 3:
                            codeChallenge = _a.sent();
                            redirectUri = AuthUtil.buildRedirectUri(true);
                            clientId = this.config.clientId;
                            locale = this.repository.getLocale();
                            return [2 /*return*/, Utils.buildAuthUrl(authorizeUrl, codeChallenge, redirectUri, clientId, locale, forceRegistration)];
                    }
                });
            });
        };
        ErogamesAuth.prototype.proceedAuth = function (code) {
            return __awaiter(this, void 0, void 0, function () {
                var redirectUri, codeVerifier, user, e_3;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            redirectUri = AuthUtil.buildRedirectUri();
                            codeVerifier = Pkce.getCodeVerifier();
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 4, 5, 6]);
                            return [4 /*yield*/, this.repository.loadToken(code, 'authorization_code', redirectUri, codeVerifier)];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, this.repository.loadUser()];
                        case 3:
                            user = _a.sent();
                            this.onAuthSuccess(user);
                            return [3 /*break*/, 6];
                        case 4:
                            e_3 = _a.sent();
                            this.onAuthError({ code: ErogamesAuth.authError, message: e_3 });
                            return [3 /*break*/, 6];
                        case 5:
                            Pkce.clear();
                            return [7 /*endfinally*/];
                        case 6: return [2 /*return*/];
                    }
                });
            });
        };
        ErogamesAuth.prototype.onAuthSuccess = function (user) {
            PrefUtil.setOnLogin(false);
            this.onAuthSuccessPending = user;
            this.onAuthErrorPending = null;
            if (this.authCallback)
                this.authCallback(user, null);
        };
        ErogamesAuth.prototype.onAuthError = function (error) {
            PrefUtil.setOnLogin(false);
            this.onAuthErrorPending = error;
            this.onAuthSuccessPending = null;
            if (this.authCallback)
                this.authCallback(null, error);
        };
        ErogamesAuth.getWhitelabelId = function () {
            var whitelabelId = Utils.getQueryValue(window.location.search, 'whitelabel', null);
            if (!whitelabelId) {
                whitelabelId = document.getElementsByTagName("body")[0]
                    .getAttribute("data-whitelabel");
            }
            if (!whitelabelId)
                whitelabelId = "erogames";
            return whitelabelId;
        };
        ErogamesAuth.authError = "auth_error";
        ErogamesAuth.authAborted = "auth_aborted";
        return ErogamesAuth;
    }());

    function auth() {
        return ErogamesAuth.getInstance();
    }

    exports.auth = auth;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
