import pkg from './package.json';
import typescript from 'typescript';
import json from '@rollup/plugin-json';
import typescriptPlugin from 'rollup-plugin-typescript2';
import {terser} from 'rollup-plugin-terser';

const deps = Object.keys(
    Object.assign({}, pkg.dependencies)
);


const buildPlugins1 = [
    typescriptPlugin({
        typescript
    }),
    json()
];

const buildPlugins2 = [
    typescriptPlugin({
        typescript,
        tsconfigOverride: {
            compilerOptions: {
                declaration: false
            }
        }
    }),
    json()
];

const buildPlugins3 = [
    typescriptPlugin({
        typescript,
        tsconfigOverride: {
            compilerOptions: {
                declaration: false
            }
        }
    }),
    terser({format: {comments: false}}),
    json()
];

const build1 = [
    {
        input: 'src/index.ts',
        output: [
            {
                file: pkg.main,
                format: 'umd',
                name: 'erogames',
                sourcemap: true,
            },
            {
                file: pkg.module,
                format: 'esm',
                sourcemap: true,
            },
        ],
        plugins: buildPlugins1,
        external: id => deps.some(dep => id === dep || id.startsWith(`${dep}/`))
    }
];

const build2 = [
    {
        input: 'src/index.ts',
        output: [
            {
                file: `erogames-auth-${pkg.version}.js`,
                format: 'umd',
                name: 'erogames',
                sourcemap: false,
            },
        ],
        plugins: buildPlugins2,
        external: id => deps.some(dep => id === dep || id.startsWith(`${dep}/`))
    }
];

const build3 = [
    {
        input: 'src/index.ts',
        output: [
            {
                file: `erogames-auth-${pkg.version}.min.js`,
                format: 'umd',
                name: 'erogames',
                sourcemap: false,
            },
        ],
        plugins: buildPlugins3,
        external: id => deps.some(dep => id === dep || id.startsWith(`${dep}/`))
    }
];

export default [...build1, ...build2, ...build3];