import {Repository} from "./repository"
import {ApiService} from "./apiService"
import {InMemoryStorage} from "./inMemoryStorage"
import {Utils} from "./utils"
import {AppConfig} from "./types/appConfig"
import {User} from "./types/user"
import {Token} from "./types/token"
import {Whitelabel} from "./types/whitelabel"
import {DataPoint} from "./types/dataPoint";
import {PrefUtil} from "./prefUtil";
import {AuthUtil} from "./authUtil";
import {QuestData} from "./types/questData";
import {LocalStorage} from "./localStorage";
import {Code} from "./types/code"
import {Error} from "./types/error"
import {PaymentInfo} from "./types/paymentInfo";
import {Pkce} from "./pkce";

type AuthCallback = (user: User | null, error: Error | null) => any

export class ErogamesAuth {
    private static instance: ErogamesAuth;

    public static readonly authError: string = "auth_error";
    public static readonly authAborted: string = "auth_aborted";

    private authCallback?: AuthCallback;
    private onAuthSuccessPending?: User | null;
    private onAuthErrorPending?: Error | null;

    private config?: AppConfig;
    private forceLogin: boolean = true;
    private readonly _accessKey = 'b63cd11ff4c45e9b20b454f5dd9a93f6';
    private repository?: Repository;

    static getInstance(): ErogamesAuth {
        if (!ErogamesAuth.instance) ErogamesAuth.instance = new ErogamesAuth();
        return ErogamesAuth.instance;
    }

    /**
     * Erogames Auth Initialization
     * @param config
     */
    async init(config: AppConfig) {
        if (!config.hasOwnProperty('clientId')) config.clientId = "";
        if (!config.hasOwnProperty('autoLogin')) config.autoLogin = true;
        this.config = config;
        this.repository = new Repository(this.config!!.clientId, new ApiService(), new InMemoryStorage(), new LocalStorage());

        if (window.name === Utils.iframeWindowName) return;

        const user = this.getUser();
        if (!user) {
            const codeValue = Utils.getQueryValue(window.location.search, "code", null);
            if (codeValue) {
                let baseUrl = Utils.removeQueryString(window.location.href, "code");
                window.history.replaceState(null, "", baseUrl);
                await this.proceedAuth(codeValue);
            } else {
                await this.login(this.repository.getLocale());
            }
        } else {
            this.onAuthSuccess(user);
        }

        this.forceLogin = this.config!!.autoLogin;
        window.onmessage = async (event: MessageEvent) => {
            if (window.origin === event.origin) {
                try {
                    const code: Code = JSON.parse(event.data);
                    if (!code?.is_erogames_js_sdk) return;
                    if (code.value) {
                        await this.proceedAuth(code.value);
                    } else {
                        if (this.forceLogin) {
                            window.location.replace(code.origin)
                        } else {
                            this.onAuthSuccess(null);
                        }
                        this.forceLogin = true;
                    }
                } catch (e) {
                    console.log("ErogamesAuth -> onmessage:" + e);
                    console.log("ErogamesAuth -> event data:" + event.data);
                }
            }
        }
    }

    /**
     * Monitors the authentication state.
     * A callback will trigger once the user has logged in, signed up, or logged out.
     *
     * @param authCallback
     */
    onAuth(authCallback: AuthCallback) {
        this.authCallback = authCallback;
        if (this.onAuthSuccessPending != null) {
            this.authCallback(this.onAuthSuccessPending, null);
            this.onAuthSuccessPending = null;
            this.onAuthErrorPending = null;
        }
        if (this.onAuthErrorPending != null) {
            this.authCallback(null, this.onAuthErrorPending);
            this.onAuthSuccessPending = null;
            this.onAuthErrorPending = null;
        }
    }

    /**
     * Starts sign in process. The page may be reloaded.
     *
     * @param lang - the preferable language of the "sign in" web page.
     */
    async login(lang: string = Utils.defaultLang): Promise<void> {
        this.repository!!.setLocale(lang);
        await this.loginInternal(false);
    }

    /**
     * Sign in by username(email)/password.
     *
     * @param username
     * @param password
     */
    async loginByPassword(username: string, password: string): Promise<void> {
        this.repository!!.setLocale(Utils.defaultLang);
        PrefUtil.setOnLogin(true);
        const whitelabelId = ErogamesAuth.getWhitelabelId();

        try {
            const jwtToken = await this.repository!!.loadJwtToken(this._accessKey);
            const whitelabel = await this.repository!!.loadWhitelabel(jwtToken, whitelabelId!!);
            await this.repository!!.loadTokenByPassword(whitelabel.token_url, username, password);
            const user = await this.repository!!.loadUser(whitelabel.profile_url);
            this.onAuthSuccess(user);
        } catch (e) {
            this.onAuthError({code: ErogamesAuth.authError, message: e});
        }
    }

    /**
     * Starts sign up process. The page may be reloaded.
     *
     * @param lang - the preferable language of the "sign in" web page.
     */
    async signup(lang: string = Utils.defaultLang): Promise<void> {
        this.repository!!.setLocale(Utils.defaultLang);
        await this.loginInternal(true);
    }

    /**
     * Register a new user
     *
     * @param clientSecret
     * @param username
     * @param password
     * @param email
     * @param checkTermsOfUse
     */
    async registerUser(clientSecret: string, username: string, password: string, email: string, checkTermsOfUse: boolean): Promise<void> {
        const whitelabelId = ErogamesAuth.getWhitelabelId();
        const jwtToken = await this.repository!!.loadJwtToken(this._accessKey);
        const whitelabel = await this.repository!!.loadWhitelabel(jwtToken, whitelabelId!!);
        await this.repository!!.registerUser(whitelabel.url, clientSecret, username, password, email, checkTermsOfUse);
    }

    /**
     * Clear all local authentication data.
     */
    logout() {
        this.repository!!.clearLocalData();
        if (this.authCallback) this.authCallback(null, null);
    }

    /**
     * Returns the current user data, otherwise null if the user is not logged in.
     */
    getUser(): User | null {
        return this.repository!!.getUser();
    }

    /**
     * Refreshes the current user data.
     */
    async reloadUser(): Promise<User> {
        const user = await this.repository!!.reloadUser();
        if (this.authCallback) this.authCallback(user, null);
        return user
    }

    /**
     * Returns the current token, otherwise null if the user is not logged in.
     */
    getToken(): Token | null {
        return this.repository!!.getToken();
    }

    /**
     * Refreshes the current token. For example, can be used if the token is expired.
     */
    async refreshToken(): Promise<Token> {
        return this.repository!!.refreshToken();
    }

    /**
     * Returns a whitelabel info.
     */
    getWhitelabel(): Whitelabel | null {
        return this.repository!!.getWhitelabel();
    }

    /**
     * Load the whitelabel. For example, can be used if the user is not logged in.
     */
    async loadWhitelabel(): Promise<Whitelabel> {
        const whitelabelId = ErogamesAuth.getWhitelabelId();
        const jwtToken = await this.repository!!.loadJwtToken(this._accessKey);
        return await this.repository!!.loadWhitelabel(jwtToken, whitelabelId!!);
    }

    /**
     * Proceed a new payment.
     *
     * @param paymentId
     * @param amount
     */
    async proceedPayment(paymentId: string, amount: number): Promise<boolean> {
        return await this.repository!!.proceedPayment(paymentId, amount);
    }

    /**
     * Create/update user data points.
     * Sending new data points will create a new entries.
     * Sending a data point that was already created before will update the previous values.
     *
     * @param dataPoints
     */
    async addDataPoints(dataPoints: Array<DataPoint>): Promise<boolean> {
        return await this.repository!!.addDataPoints(dataPoints);
    }

    /**
     * Loads current quest info.
     * Load key quest statistics and fields such as quest title and
     * description.The data returned also has information about the current ranking of
     * user's clan (user is loaded based on used auth token) and also current user's
     * individual contribution to the clan score.This endpoint allows implementing in-game
     * quest status widgets for more immersive gameplay.
     */
    async loadCurrentQuest(): Promise<QuestData> {
        return await this.repository!!.getCurrentQuestData();
    }

    /**
     * Loads payment information.
     * @param paymentId
     */
    async loadPaymentInfo(paymentId: String): Promise<PaymentInfo> {
        return await this.repository!!.getPaymentInfo(paymentId);
    }

    /**
     * Check if the sign in/up process is going yet.
     */
    isLoginGoing(): boolean {
        return PrefUtil.isOnLogin();
    }

    private async loginInternal(forceRegistration: boolean): Promise<void> {
        PrefUtil.setOnLogin(true);
        const authUrl = await this.getAuthUrl(forceRegistration);
        Utils.getCodeFromIframe(authUrl);
    }

    private async getAuthUrl(forceRegistration: boolean): Promise<string> {
        const whitelabelId = ErogamesAuth.getWhitelabelId();
        const jwtToken = await this.repository!!.loadJwtToken(this._accessKey);
        const whitelabel = await this.repository!!.loadWhitelabel(jwtToken, whitelabelId!!);

        const authorizeUrl = whitelabel!!.authorize_url;
        const codeChallenge = await Pkce.generateCodeChallenge(Pkce.getCodeVerifier())
        const redirectUri = AuthUtil.buildRedirectUri(true);
        const clientId = this.config!!.clientId;
        const locale = this.repository!!.getLocale();
        return Utils.buildAuthUrl(
            authorizeUrl,
            codeChallenge,
            redirectUri,
            clientId!!,
            locale,
            forceRegistration
        );
    }

    private async proceedAuth(code: string): Promise<void> {
        const redirectUri = AuthUtil.buildRedirectUri();
        const codeVerifier = Pkce.getCodeVerifier();
        try {
            await this.repository!!.loadToken(code, 'authorization_code', redirectUri, codeVerifier);
            const user = await this.repository!!.loadUser();
            this.onAuthSuccess(user);
        } catch (e) {
            this.onAuthError({code: ErogamesAuth.authError, message: e});
        } finally {
            Pkce.clear();
        }
    }

    private onAuthSuccess(user: User | null) {
        PrefUtil.setOnLogin(false);
        this.onAuthSuccessPending = user;
        this.onAuthErrorPending = null;
        if (this.authCallback) this.authCallback(user, null);
    }

    private onAuthError(error: Error | null) {
        PrefUtil.setOnLogin(false);
        this.onAuthErrorPending = error;
        this.onAuthSuccessPending = null;
        if (this.authCallback) this.authCallback(null, error);
    }

    private static getWhitelabelId() {
        let whitelabelId = Utils.getQueryValue(window.location.search, 'whitelabel', null);
        if (!whitelabelId) {
            whitelabelId = document.getElementsByTagName("body")[0]
                .getAttribute("data-whitelabel");
        }
        if (!whitelabelId) whitelabelId = "erogames";

        return whitelabelId
    }
}