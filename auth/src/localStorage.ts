import {IStorage} from "./iStorage";
import {Whitelabel} from "./types/whitelabel";
import {Token} from "./types/token";
import {User} from "./types/user";

export class LocalStorage implements IStorage {

    private readonly STORAGE_WHITELABEL_KEY = "ero_sdk_auth_storage_whitelabel_key";
    private readonly STORAGE_TOKEN_KEY = "ero_sdk_auth_storage_token_key";
    private readonly STORAGE_USER_KEY = "ero_sdk_auth_storage_user_key";
    private readonly STORAGE_LANG_KEY = "ero_sdk_auth_storage_key";

    setWhitelabel(whitelabel: Whitelabel) {
        window.localStorage.setItem(this.STORAGE_WHITELABEL_KEY, JSON.stringify(whitelabel));
    }

    getWhitelabel(): Whitelabel | null {
        return JSON.parse(<string>window.localStorage.getItem(this.STORAGE_WHITELABEL_KEY)) as Whitelabel;
    }

    setToken(token: Token) {
        window.localStorage.setItem(this.STORAGE_TOKEN_KEY, JSON.stringify(token));
    }

    getToken(): Token {
        return JSON.parse(<string>window.localStorage.getItem(this.STORAGE_TOKEN_KEY));
    }

    setUser(user: User) {
        window.localStorage.setItem(this.STORAGE_USER_KEY, JSON.stringify(user));
    }

    getUser(): User | null {
        return JSON.parse(<string>window.localStorage.getItem(this.STORAGE_USER_KEY));
    }

    removeUser() {
        window.localStorage.removeItem(this.STORAGE_USER_KEY);
    }

    removeToken() {
        window.localStorage.removeItem(this.STORAGE_TOKEN_KEY);
    }

    removeWhitelabel() {
        window.localStorage.removeItem(this.STORAGE_WHITELABEL_KEY);
    }

    setLocale(lang: string) {
        window.localStorage.setItem(this.STORAGE_LANG_KEY, lang);
    }

    getLocale(): string | null {
        return window.localStorage.getItem(this.STORAGE_LANG_KEY);
    }
}