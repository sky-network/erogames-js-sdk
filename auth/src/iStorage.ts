import {Whitelabel} from "./types/whitelabel";
import {Token} from "./types/token";
import {User} from "./types/user";

export interface IStorage {

    setWhitelabel(whitelabel: Whitelabel): void

    getWhitelabel(): Whitelabel | null

    setToken(token: Token): void

    getToken(): Token | null

    setUser(user: User): void

    getUser(): User | null

    removeUser(): void

    removeToken(): void

    removeWhitelabel(): void

    setLocale(lang: string): void

    getLocale(): string | null
}