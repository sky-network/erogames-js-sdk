import {IStorage} from "./iStorage";
import {Whitelabel} from "./types/whitelabel";
import {Token} from "./types/token";
import {User} from "./types/user";

export class InMemoryStorage implements IStorage {

    private locale: string | null = null;
    private token: Token | null = null;
    private user: User | null = null;
    private whitelabel: Whitelabel | null = null;

    setLocale(lang: string): void {
        this.locale = lang;
    }

    getLocale(): string | null {
        return this.locale;
    }

    setToken(token: Token): void {
        this.token = token;
    }

    getToken(): Token | null {
        return this.token;
    }

    removeToken(): void {
        this.token = null;
    }

    setUser(user: User): void {
        this.user = user;
    }

    getUser(): User | null {
        return this.user;
    }

    removeUser(): void {
        this.user = null;
    }

    setWhitelabel(whitelabel: Whitelabel): void {
        this.whitelabel = whitelabel;
    }

    getWhitelabel(): Whitelabel | null {
        return this.whitelabel;
    }

    removeWhitelabel(): void {
        this.whitelabel = null;
    }
}