import {IStorage} from "./iStorage";
import {ApiService} from "./apiService";
import {Whitelabel} from "./types/whitelabel";
import {Token} from "./types/token";
import {User} from "./types/user";
import {DataPoint} from "./types/dataPoint";
import {Utils} from "./utils";
import {QuestData} from "./types/questData";
import {PaymentInfo} from "./types/paymentInfo";

export class Repository {

    private readonly clientId: string;
    private apiService: ApiService;
    private inMemoryStorage: IStorage;
    private localStorage: IStorage;

    constructor(clientId: string, apiService: ApiService, inMemoryStorage: IStorage, localStorage: IStorage) {
        this.clientId = clientId;
        this.apiService = apiService;
        this.inMemoryStorage = inMemoryStorage;
        this.localStorage = localStorage;
    }

    async loadJwtToken(accessKey: string): Promise<string> {
        return this.apiService.loadJwtToken(accessKey);
    }

    setWhitelabel(whitelabel: Whitelabel) {
        this.localStorage.setWhitelabel(whitelabel);
    }

    getWhitelabel(): Whitelabel | null {
        return this.localStorage.getWhitelabel();
    }

    async loadWhitelabel(jwtToken: string, whitelabelId: string): Promise<Whitelabel> {
        const whitelabels = await this.loadWhitelabels(jwtToken);
        if (whitelabelId) {
            let whitelabel: Whitelabel | undefined = whitelabels.find(element => element.slug === whitelabelId);
            if (!whitelabel) {
                throw `The whitelabel ${whitelabelId} does not exist.`;
            }
            console.log('current wl: ' + whitelabel.slug);
            this.setWhitelabel(whitelabel);
            return whitelabel;
        }
        throw `Invalid whitelabel ${whitelabelId}.`;
    }

    async loadWhitelabels(jwtToken: string): Promise<Array<Whitelabel>> {
        return this.apiService.getWhitelabels(jwtToken);
    }

    async loadToken(code: string, grantType: string, redirectUri: string, codeVerifier: string) {
        const whitelabel = this.getWhitelabel();
        if (!whitelabel) throw 'The whitelabel can\'t be null.';

        let token = await this.apiService.getToken(
            whitelabel.token_url,
            this.clientId,
            code,
            grantType,
            redirectUri,
            codeVerifier
        );
        this.setToken(token);
        return token;
    }

    async loadUser(profileUrl: string | null = null): Promise<User> {
        let token = await this.getOrRefreshToken();
        if (!token) throw ("The token can't be null.");
        let whitelabel: Whitelabel | null = null;
        if (!profileUrl) {
            whitelabel = this.getWhitelabel();
            if (!whitelabel) throw ("The whitelabel can't be null.");
        }

        if (!profileUrl) {
            profileUrl = whitelabel!!.profile_url;
        }

        const user = await this.apiService.getUser(profileUrl!!, `Bearer ${token.access_token}`);
        this.setUser(user);
        return user;
    }

    async reloadUser(): Promise<User> {
        let token = await this.getOrRefreshToken();
        const whitelabel = this.getWhitelabel();
        if (!token || !whitelabel) throw 'The token and/or whitelabel can\'t be null.';

        const user = await this.apiService.getUser(whitelabel.profile_url, `Bearer ${token.access_token}`);
        this.setUser(user);
        return user;
    }

    setToken(token: Token) {
        this.inMemoryStorage.setToken(token);
    }

    getToken() {
        return this.inMemoryStorage.getToken();
    }

    setUser(user: User) {
        this.inMemoryStorage.setUser(user);
    }

    getUser() {
        return this.inMemoryStorage.getUser();
    }

    clearLocalData() {
        this.removeUser();
        this.removeToken();
        this.removeWhitelabel();
    }

    private removeUser() {
        this.inMemoryStorage.removeUser();
        this.localStorage.removeUser();
    }

    private removeToken() {
        this.inMemoryStorage.removeToken();
        this.localStorage.removeUser();
    }

    private removeWhitelabel() {
        this.inMemoryStorage.removeWhitelabel();
        this.localStorage.removeUser();
    }

    async refreshToken(): Promise<Token> {
        const token = this.getToken();
        const whitelabel = this.getWhitelabel();
        if (!token) throw ("The token can't be null.");
        if (!whitelabel) throw ("The whitelabel can't be null.");

        const refreshedToken = await this.apiService.refreshToken(
            whitelabel.token_url,
            this.clientId,
            token.refresh_token
        );
        this.setToken(refreshedToken);
        return refreshedToken;
    }

    private async getOrRefreshToken(): Promise<Token | null> {
        const token = await this.getToken();
        if (token != null && Repository.isTokenExpired(token)) return await this.refreshToken()
        return token;
    }

    async proceedPayment(paymentId: string, amount: number): Promise<boolean> {
        let token = await this.getOrRefreshToken();
        const whitelabel = this.getWhitelabel();
        if (!token) throw ("The token can't be null.");
        if (!whitelabel) throw ("The whitelabel can't be null.");

        return await this.apiService.proceedPayment(
            whitelabel.url + "/api/v1/payments",
            `Bearer ${token.access_token}`,
            paymentId,
            amount
        );
    }

    async addDataPoints(dataPoints: Array<DataPoint>): Promise<boolean> {
        let token = await this.getOrRefreshToken();
        const whitelabel = this.getWhitelabel();
        if (!token) throw ("The token can't be null.");
        if (!whitelabel) throw ("The whitelabel can't be null.");

        return await this.apiService.addDataPoints(
            whitelabel.url + "/api/v1/me/data_point_collection",
            `Bearer ${token.access_token}`,
            dataPoints
        );
    }

    async getCurrentQuestData(): Promise<QuestData> {
        let token = await this.getOrRefreshToken();
        const whitelabel = this.getWhitelabel();
        if (!token) throw ("The token can't be null.");
        if (!whitelabel) throw ("The whitelabel can't be null.");

        return await this.apiService.getCurrentQuestData(
            whitelabel.url + "/api/v1/quests/current",
            `Bearer ${token.access_token}`
        );
    }

    async getPaymentInfo(paymentId: String): Promise<PaymentInfo> {
        let token = await this.getOrRefreshToken();
        const whitelabel = this.getWhitelabel();
        if (!token) throw ("The token can't be null.");
        if (!whitelabel) throw ("The whitelabel can't be null.");

        return await this.apiService.getPaymentInfo(
            whitelabel.url + "/api/v1/payments/" + paymentId,
            `Bearer ${token.access_token}`
        );
    }

    setLocale(lang: string) {
        const l = (Utils.supportedLanguages.indexOf(lang) > -1)
            ? lang : Utils.defaultLang;
        this.localStorage.setLocale(l);
    }

    getLocale(): string {
        const l = this.localStorage.getLocale();
        if (l) return l;
        return Utils.defaultLang;
    }

    async loadTokenByPassword(tokenUrl: any, username: string, password: string): Promise<Token> {
        const token = await this.apiService.getTokenByPassword(tokenUrl, this.clientId, username, password);
        this.setToken(token);
        return token;
    }

    async registerUser(baseUrl: string, clientSecret: string, username: string, password: string, email: string, checkTermsOfUse: boolean): Promise<void> {
        await this.apiService.registerUser(baseUrl, this.clientId, clientSecret, username, password, email, checkTermsOfUse);
    }

    private static isTokenExpired(token: Token) {
        const timestamp = Date.now() / 1000;
        return (timestamp + 10) > (token.created_at + token.expires_in);
    }
}