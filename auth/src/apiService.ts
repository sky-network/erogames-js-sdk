import {Whitelabel} from "./types/whitelabel";
import {Token} from "./types/token";
import {User} from "./types/user";
import {Utils} from "./utils";
import {DataPoint} from "./types/dataPoint";
import {QuestData} from "./types/questData";
import {PaymentInfo} from "./types/paymentInfo";

export class ApiService {

    private readonly _baseUrl = 'https://erogames.com/';
    private readonly _jwtEndpoint = 'api/v1/authenticate';
    private readonly _whitelabelsEndpoint = 'api/v1/whitelabels';
    private readonly _registerUserEndpoint = 'api/v1/register';

    private readonly _jsonHeaders: any = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };

    async loadJwtToken(accessKey: string): Promise<string> {
        const bodyData = {
            "access_key": accessKey
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(this._baseUrl + this._jwtEndpoint, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData['token'] as string;
    }

    async getWhitelabels(jwtToken: string): Promise<Array<Whitelabel>> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = jwtToken;
        const settings = {
            method: 'GET',
            headers: headers,
        };
        const response = await fetch(this._baseUrl + this._whitelabelsEndpoint, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData['whitelabels'] as Array<Whitelabel>;
    }

    async getUser(profileUrl: string, accessToken: string): Promise<User> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = accessToken;
        const settings = {
            method: 'GET',
            headers: headers,
        };
        const response = await fetch(profileUrl, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData['user'];
    }

    async getToken(url: string, clientId: string, code: string, grantType: string, redirectUri: string, codeVerifier: string): Promise<Token> {
        const bodyData = {
            "client_id": clientId,
            "code": code,
            "grant_type": grantType,
            "redirect_uri": redirectUri,
            "code_verifier": codeVerifier
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData;
    }

    async refreshToken(tokenUrl: string, clientId: string | null, refreshToken: string): Promise<Token> {
        const bodyData = {
            "client_id": clientId,
            "grant_type": 'refresh_token',
            "refresh_token": refreshToken,
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(tokenUrl, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData;
    }

    async proceedPayment(url: string, token: string, paymentId: string, amount: number): Promise<boolean> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = token
        const bodyData = {
            "paymentId": paymentId,
            "amount": amount,
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return true;
    }

    async addDataPoints(url: string, token: string, dataPoints: Array<DataPoint>): Promise<boolean> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = token;

        const bodyData = {
            data_points: dataPoints
        };

        const settings = {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return true;
    }

    async getCurrentQuestData(url: string, token: string): Promise<QuestData> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = token;
        const settings = {
            method: 'GET',
            headers: headers,
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData;
    }

    async getPaymentInfo(url: string, token: string): Promise<PaymentInfo> {
        let headers = this._jsonHeaders;
        headers['Authorization'] = token;
        const settings = {
            method: 'GET',
            headers: headers,
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData;
    }

    async getTokenByPassword(url: any, clientId: any, username: string, password: string): Promise<Token> {
        const bodyData = {
            "client_id": clientId,
            "grant_type": "password",
            "login": username,
            "password": password,
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };
        const response = await fetch(url, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
        return responseData;
    }

    async registerUser(baseUrl: string, clientId: string, clientSecret: string, username: string, password: string, email: string, checkTermsOfUse: boolean): Promise<void> {
        const bodyData = {
            "client_id": clientId,
            "client_secret": clientSecret,
            "username": username,
            "password": password,
            "passwordConfirmation": password,
            "email": email,
            "checkTermsOfUse": checkTermsOfUse,
        };
        const settings = {
            method: 'POST',
            headers: this._jsonHeaders,
            body: JSON.stringify(bodyData)
        };

        if (!baseUrl.endsWith("/")) baseUrl = baseUrl + "/";
        const response = await fetch(baseUrl + this._registerUserEndpoint, settings);
        const responseData = await response.json();
        if (!Utils.isResponseSuccessful(response)) throw Utils.extractErrorMessage(responseData);
    }
}