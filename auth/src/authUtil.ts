import {Utils} from "./utils";

export class AuthUtil {

    static buildRedirectUri(encode: boolean = false): string {
        let url: string = window.location.href;
        url = Utils.removeQueryString(url, "code");
        if (encode) url = encodeURIComponent(url);
        return url;
    }
}