export interface QuestData {
    quest: Quest;
    leaders: Leader[];
    user_clan_attempt: UserClanAttempt | null;
    user_score: UserScore;
}

interface Quest {
    id: number;
    title: string;
    description: string | null;
    cover_url: string | null;
    finished_at: string;
}

interface Leader {
    position: number;
    name: string;
    cover_picture_url: string | null;
    clan_leader_name: string | null;
    score: number;
}

interface UserClanAttempt {
    position: number;
    name: string;
    cover_picture_url: string;
    clan_leader_name: string;
    score: number;
}

interface UserScore {
    total: number;
    earned_points: EarnedPoint[];
}

interface EarnedPoint {
    points: number;
    title: string;
}