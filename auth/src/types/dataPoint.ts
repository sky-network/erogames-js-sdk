export interface DataPoint {
    field: string;
    value: string;
}