export interface User {
    id: number;
    email?: string;
    username: string;
    avatar_url?: string;
    language?: string;
    balance: number;
}