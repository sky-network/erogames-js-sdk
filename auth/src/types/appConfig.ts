export interface AppConfig {
    clientId: string;
    autoLogin: boolean;
}