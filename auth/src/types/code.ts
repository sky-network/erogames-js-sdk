export interface Code {
    value: string | null;
    origin: string;
    is_erogames_js_sdk: boolean;
}