export interface PaymentInfo {
    payment_id: string;
    debit_amount: number;
}