export interface Whitelabel {
    slug: string;
    name: string;
    url: string;
    logo_url?: string;
    authorize_url: string;
    token_url: string;
    profile_url: string;
    buy_erogold_url: object; //Map<string, string>
    support_url: object; //Map<string, string>
}