export class Pkce {
    private static readonly CODE_VERIFIER_KEY = "ero_sdk_auth_storage_code_verifier_key";

    private constructor() {
    }

    static getCodeVerifier(generateNew = false): string {
        let codeVerifier = window.localStorage.getItem(Pkce.CODE_VERIFIER_KEY);
        if (codeVerifier == null || generateNew) {
            codeVerifier = this.generateCodeVerifier();
            window.localStorage.setItem(Pkce.CODE_VERIFIER_KEY, codeVerifier);
        }
        return codeVerifier;
    }

    private static generateCodeVerifier(): string {
        const array = new Uint32Array(28);
        window.crypto.getRandomValues(array);
        return Array.from(array, dec => ('0' + dec.toString(16)).substr(-2)).join('');
    }

    static async generateCodeChallenge(codeVerifier: string): Promise<string> {
        const hashed: ArrayBuffer = await this.sha256(codeVerifier);
        return this.base64URLEncode(hashed);
    }

    static clear(): void {
        window.localStorage.removeItem(Pkce.CODE_VERIFIER_KEY);
    }

    private static base64URLEncode(str: ArrayBuffer): string {
        // @ts-ignore
        return btoa(String.fromCharCode.apply(null, new Uint8Array(str)))
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=+$/, '');
    }

    private static async sha256(text: string): Promise<ArrayBuffer> {
        const encoder = new TextEncoder();
        const data = encoder.encode(text);
        return await window.crypto.subtle.digest('SHA-256', data);
    }
}