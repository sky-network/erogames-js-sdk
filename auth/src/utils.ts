export class Utils {

    static readonly defaultLang: string = 'en';
    static readonly popupWindowName: string = 'ErogamesOAuth';
    static readonly iframeWindowName: string = 'com_erogames_sdk_auth_iframe';
    static readonly supportedLanguages: Array<string> = ['en', 'fr', 'zh', 'ja', 'es'];

    private constructor() {
    }

    static buildAuthUrl(
        authorizeUrl: string,
        codeChallenge: string,
        redirectUri: string,
        clientId: string,
        locale: string,
        forceRegistration: boolean,
    ): string {
        let delimiter: string = (authorizeUrl.indexOf('?') !== -1) ? '&' : '?';
        return `${authorizeUrl}${delimiter}` +
            `code_challenge=${codeChallenge}` +
            `&redirect_uri=${redirectUri}` +
            `&client_id=${clientId}` +
            `&locale=${locale}` +
            `&force_registration=${forceRegistration}` +
            `&code_challenge_method=S256` +
            `&response_type=code` +
            `&disclaimer=none`;
    }

    static getQueryValue(locationSearch: string, queryParam: string, defaultValue: string | null): string | null {
        queryParam = queryParam.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        const regex = new RegExp('[\\?&]' + queryParam + '=([^&#]*)');
        const results = regex.exec(locationSearch);
        return results === null ? defaultValue : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    static buildLogoutUrl(url: string, token: string, redirectUri: string) {
        return `${url}/logout?token=${token}&redirect_uri=${redirectUri}`;
    }

    static isResponseSuccessful(response: Response): boolean {
        return (response.status >= 200 && response.status <= 299);
    }

    static getCodeFromIframe(url: string): void {
        let codeValue: string | null = null;
        let iframe = document.createElement("iframe");
        iframe.name = this.iframeWindowName;
        iframe.style.display = "none";
        iframe.onload = function () {
            try {
                let queryParams = iframe.contentWindow?.location.search;
                codeValue = Utils.getQueryValue(queryParams!!, "code", null);
            } catch (e) {
                console.log(e);
            }

            const payload = {value: codeValue, origin: url, is_erogames_js_sdk: true};
            iframe.contentWindow?.parent.postMessage(JSON.stringify(payload), window.origin);
            iframe.parentNode?.removeChild(iframe);
        }
        iframe.src = url;
        document.body.appendChild(iframe);
    }

    private static delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    static extractErrorMessage(rawMsg: any): string | null {
        try {
            if (rawMsg["message"]) {
                return rawMsg["message"];
            }

            if (rawMsg["error_description"]) {
                return rawMsg["error_description"];
            }

            if (rawMsg["errors"]) {
                return rawMsg["errors"].join(",");
            }
        } catch (e) {
            console.log(e);
            return null;
        }
        return null;
    }

    static removeQueryString(uri: string, param: string) {
        if (!uri) return uri;
        if (!Utils.isUrlValid(uri)) return uri;
        let url = new URL(uri);
        let regex = new RegExp('[?&]' + param + '=[^&]+');
        let newSearch = url.search.replace(regex, '').replace(/^&/, '?');
        return `${url.protocol}//${url.host}${url.pathname}${newSearch}`;
    }

    private static isUrlValid(url: string) {
        try {
            new URL(url);
        } catch (e) {
            console.log("Invalid URL: " + url);
            return false;
        }
        return true;
    }
}