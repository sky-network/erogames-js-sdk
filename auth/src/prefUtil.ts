export class PrefUtil {

    static readonly storageOnLoginKey = "ero_sdk_pref_on_login";

    private constructor() {
    }

    static setOnLogin(onLogin: boolean) {
        window.localStorage.setItem(PrefUtil.storageOnLoginKey, onLogin ? "1" : "0");
    }

    static isOnLogin(): boolean {
        const isOnLoginStr = window.localStorage.getItem(PrefUtil.storageOnLoginKey);
        const isOnLoginInt = parseInt(isOnLoginStr ?? "");
        if (isOnLoginInt) {
            return isOnLoginInt > 0;
        }
        return false;
    }
}