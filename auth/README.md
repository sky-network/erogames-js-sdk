# JS Erogames Auth

## How to add the script to a project

1. Download the script:  
   `https://gitlab.com/sky-network/erogames-js-sdk/-/blob/master/auth/erogames-auth-<x.y.z>.js`
2. Put downloaded script into the project.
3. Add the script to the `<head>` tag:
   ```html
   <head>
   ....
    <script src="path_to_the_script/erogames-auth-<x.y.z>.min.js"></script>   
   ....
   </head>
   ```
4. Initialize Erogames Auth:
   ```html
   <head>
   ....
    <script>
        const eroAuth = erogames.auth()
        eroAuth.init({clientId: 'your_client_id'})
   </script>   
   ....
   </head>
   ```

## Important!

The login process starts automatically once the user launches the app.  
Use a callback to check if a user is logged in:

```javascript
eroAuth.onAuth(callback)
```

Since an app page may reload during authentication it's recommended to start the initialization of an app (loading of
some data, etc.) after a successful login.

### Log in

```javascript
eroAuth.login()
```

### Sign up

```javascript
eroAuth.signup('fr')
```

### Log out

```javascript
// The local data will be cleaned
eroAuth.logout()
```

### Get user

```javascript
eroAuth.getUser()
```

### Reload user

```javascript
eroAuth.reloadUser().then((user) => {
    // success
}).catch((error) => {
    // error
})
```

### Get token

```javascript
eroAuth.getToken()
```

### Refresh token

```javascript
eroAuth.refreshToken().then((token) => {
    // success
}).catch((error) => {
    // error
})
```

### Whitelabel info

All necessary data such as base white-label site URL, logo URL, "buy Erogold" URL, etc.) can be taken as follows:

```javascript
eroAuth.getWhitelabel()
```

If a user is not logged in and there is a need for the above data, a whitelabel info should be loaded first:

```javascript
eroAuth.loadWhitelabel().then((whitelabel) => {
    // success
}).catch((error) => {
    // error
})
```

Get "Buy Erogold" URL example:

```javascript
const whitelabel = eroAuth.getWhitelabel()
if (whitelabel) {
    const langKey = Object.keys(wl.buy_erogold_url)[0]
    const buyErogoldUrl = whitelabel.buy_erogold_url[langKey]
}
```

### Proceed payment

```javascript
const paymentId = 'payment_uuid'
const amount = 1
eroAuth.proceedPayment(paymentId, amount).then((resp) => {
    // success
}).catch((error) => {
    // error
})
```

### Add data points

```javascript
const dataPoints = [
    {field: 'here', value: 'be'},
    {field: 'dragons', value: '5'},
    {field: 'current_experience', value: '12500'},
]

eroAuth.addDataPoints(dataPoints).then((resp) => {
    // success
}).catch((error) => {
    // error
})
```

### Load current quest info:

```javascript
eroAuth.loadCurrentQuest().then((guestData) => {
    // success
}).catch((error) => {
    // error
})
```

### Load payment info:

```javascript
eroAuth.loadPaymentInfo(paymentId).then((paymentInfo) => {
    // success
}).catch((error) => {
    // error
})
```

## Demo app

```shell
npm run build
npm run demo
```


