const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

app.use(
    '/node_modules',
    express.static(path.resolve(__dirname, '../node_modules'))
);
app.use(express.static(__dirname));


app.get('/auth.umd.js', function(req, res) {
    res.sendFile(__dirname + '/public/dist/index.umd.js');
});
app.get('/index.umd.js.map', function(req, res) {
    res.sendFile(__dirname + '/public/dist/index.umd.js.map');
});

app.get('/auth.esm.js', function(req, res) {
    res.sendFile(__dirname + '/public/dist/index.esm.js');
});
app.get('/index.esm.js.map', function(req, res) {
    res.sendFile(__dirname + '/public/dist/index.esm.js.map');
});


app.get('/', (req, res) => {
    res.sendFile('./index.html', { root: __dirname });
});
app.listen(port, () => {
    console.log(`Demo app is here: http://localhost:${port}`)
})